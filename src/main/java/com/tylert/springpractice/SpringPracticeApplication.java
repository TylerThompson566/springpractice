package com.tylert.springpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Start of execution
 * 
 * @author tyler
 *
 */
@SpringBootApplication
@PropertySource("classpath:/static/rates.properties")
public class SpringPracticeApplication {

	/**
	 * Main method, start of execution
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringPracticeApplication.class, args);
	}
}
