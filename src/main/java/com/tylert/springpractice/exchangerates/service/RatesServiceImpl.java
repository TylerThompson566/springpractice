package com.tylert.springpractice.exchangerates.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tylert.springpractice.exchangerates.model.ConversionResponseModel;
import com.tylert.springpractice.exchangerates.model.RateModel;
import com.tylert.springpractice.exchangerates.model.RatesModel;
import com.tylert.springpractice.exchangerates.model.RatesResponseModel;

/**
 * Service that implements the calls to the currency exchange API
 * 
 * @author tyler
 *
 */
@Service
public class RatesServiceImpl implements RatesService {

	/** The logger */
	private static final Logger log = LoggerFactory.getLogger(RatesServiceImpl.class);

	/** the Base URL for the exchange rate API */
	@Value("${rates.baseUrl}")
	private String baseUrl;

	/** The default base to convert from */
	@Value("${rates.defaultBase}")
	private String defaultBase;

	/** The rest template for making rest calls */
	@Autowired
	private RestTemplate restTemplate;

	/** the object mapper from Jackson to convert to and from JSON and POJOs */
	@Autowired
	private ObjectMapper objectMapper;

	/**
	 * Get the conversion rates based on the given base
	 * 
	 * @param base   the base to base all conversion rates from
	 * @param amount the amount of the base currency we are checking
	 * @return the RatesResponseModel object populated with all latest conversion
	 *         rates
	 * @throws JsonProcessingException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	public RatesResponseModel getRates(String base, double amount)
			throws JsonProcessingException, InterruptedException, ExecutionException {

		// get the url for getting the rates
		String url = baseUrl + "latest?base=" + (base.equals("") ? defaultBase : base);
		log.info("Making GET request to {}", url);

		// get the model object asynchronously
		CompletableFuture<RatesModel> ratesCompletableFuture = CompletableFuture
				.supplyAsync(() -> restTemplate.getForObject(url, RatesModel.class));
		RatesModel responseModel = ratesCompletableFuture.get();

		// remove the base rate from the map, we don't need it since will always be
		// 1:1
		responseModel.getRates().remove(base.equals("") ? defaultBase : base);

		// logging
		if (log.isDebugEnabled()) {
			String jsonRepresentation = objectMapper.writeValueAsString(responseModel);
			log.debug("response: {}", jsonRepresentation);
		}

		// convert the map in the response into a list of RateModel objects
		ArrayList<RateModel> rates = new ArrayList<>();

		// use streams to iterate over the map and add the rates to the list
		responseModel.getRates().entrySet().stream().forEach(e -> rates.add(new RateModel(e.getKey(), (e.getValue() * amount))));

		// return the RatesResponseModel
		return new RatesResponseModel(responseModel.getBase(), amount, responseModel.getDate(), rates);
	}

	/**
	 * Get a strict conversion from one currency to another
	 * 
	 * @param from   the currency we are converting from
	 * @param to     the currency we are converting to
	 * @param amount the amount we are converting
	 * @return the ConversionResponseModel representing the conversion result
	 * @throws ExecutionException
	 * @throws InterruptedException
	 * @throws JsonProcessingException
	 */
	public ConversionResponseModel getConversion(String from, String to, double amount)
			throws InterruptedException, ExecutionException, JsonProcessingException {

		// get the url for getting the rates
		String url = baseUrl + "latest/?base=" + (from.equals("") ? defaultBase : from) + "&symbols=" + to;
		log.info("Making GET request to {}", url);

		// get the model object asynchronously
		CompletableFuture<RatesModel> completableFuture = CompletableFuture
				.supplyAsync(() -> restTemplate.getForObject(url, RatesModel.class));
		RatesModel responseModel = completableFuture.get();

		// logging
		if (log.isDebugEnabled()) {
			String jsonRepresentation = objectMapper.writeValueAsString(responseModel);
			log.debug("response: {}", jsonRepresentation);
		}

		// return the conversion response model
		return new ConversionResponseModel((from.equals("") ? defaultBase : from),
				(String) responseModel.getRates().keySet().toArray()[0], amount,
				(amount * responseModel.getRates().get((String) responseModel.getRates().keySet().toArray()[0])),
				responseModel.getDate());
	}
}
