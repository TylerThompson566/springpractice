package com.tylert.springpractice.exchangerates.service;

import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tylert.springpractice.exchangerates.model.ConversionResponseModel;
import com.tylert.springpractice.exchangerates.model.RatesResponseModel;

/**
 * Interface that defines the methods needed for RatesService
 * 
 * @author tyler
 *
 */
public interface RatesService {

	public abstract RatesResponseModel getRates(String base, double amount) throws InterruptedException, ExecutionException, JsonProcessingException;
	public abstract ConversionResponseModel getConversion(String from, String to, double amount) throws InterruptedException, ExecutionException, JsonProcessingException;
}
