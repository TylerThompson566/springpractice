package com.tylert.springpractice.exchangerates.controller;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tylert.springpractice.exchangerates.model.ConversionResponseModel;
import com.tylert.springpractice.exchangerates.model.RatesResponseModel;
import com.tylert.springpractice.exchangerates.service.RatesService;

/**
 * Rest Controller for handling requests for exchange rates
 * 
 * @author tyler
 *
 */
@RestController
public class RatesController {

	/**
	 * The Rates Service class that provides the rates controller with business
	 * logic
	 */
	@Autowired
	private RatesService ratesService;

	/**
	 * get the currency exchange rates from USD
	 * 
	 * @return the RatesModel object that is a representation of the response
	 * @throws JsonProcessingException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@GetMapping("/rates")
	public RatesResponseModel getRates(@RequestParam(value = "base", defaultValue = "") String base, @RequestParam(value = "amount", defaultValue = "1") double amount)
			throws JsonProcessingException, InterruptedException, ExecutionException {

		// return the result of the service call
		return ratesService.getRates(base, amount);
	}

	/**
	 * get the conversion rate from
	 * 
	 * @return the ConversionResponseModel
	 * @throws JsonProcessingException
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@GetMapping("/conversion")
	public ConversionResponseModel getConversion(@RequestParam(value = "from", defaultValue = "") String from,
			@RequestParam String to, @RequestParam double amount)
			throws JsonProcessingException, InterruptedException, ExecutionException {

		// return the result of the service call
		return ratesService.getConversion(from, to, amount);
	}
}
