package com.tylert.springpractice.exchangerates.model;

import java.util.Map;

/**
 * Model representing the response from the exchangeratesapi.io API
 * 
 * @author tyler
 *
 */
public class RatesModel {

	/** The base for currency conversion */
	private String base;

	/** The map of rates */
	private Map<String, Double> rates;

	/** The date the request was made on */
	private String date;

	/**
	 * Constructor
	 * 
	 * @param base  The base for currency conversion
	 * @param rates The map of rates
	 * @param date  The date the request was made on
	 */
	public RatesModel(String base, Map<String, Double> rates, String date) {
		this.base = base;
		this.rates = rates;
		this.date = date;
	}

	// ACCESSORS
	public String getBase() {
		return base;
	}

	public Map<String, Double> getRates() {
		return rates;
	}

	public String getDate() {
		return date;
	}

	// MUTATORS
	public void setBase(String base) {
		this.base = base;
	}

	public void setRates(Map<String, Double> rates) {
		this.rates = rates;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
