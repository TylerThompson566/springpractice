package com.tylert.springpractice.exchangerates.model;

/**
 * Model from the conversion response
 * 
 * @author tyler
 *
 */
public class ConversionResponseModel {

	/** the currency we are converting from */
	private String from;

	/** the currency we are converting to */
	private String to;

	/** the amount we are converting from */
	private double fromAmount;

	/** the amount we are converting to */
	private double toAmount;

	/** the date the call was made */
	private String date;

	/**
	 * Constructor
	 * 
	 * @param from       the currency we are converting from
	 * @param to         the currency we are converting to
	 * @param fromAmount the amount we are converting from
	 * @param toAmount   the amount we are converting to
	 */
	public ConversionResponseModel(String from, String to, double fromAmount, double toAmount, String date) {
		this.from = from;
		this.to = to;
		this.fromAmount = fromAmount;
		this.toAmount = toAmount;
		this.date = date;
	}

	// ACCESSORS
	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public double getFromAmount() {
		return fromAmount;
	}

	public double getToAmount() {
		return toAmount;
	}

	public String date() {
		return date;
	}

	// MUTATORS
	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setFromAmount(double fromAmount) {
		this.fromAmount = fromAmount;
	}

	public void setToAmount(double toAmount) {
		this.toAmount = toAmount;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
