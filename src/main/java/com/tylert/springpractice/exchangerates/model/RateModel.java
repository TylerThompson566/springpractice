package com.tylert.springpractice.exchangerates.model;

/**
 * The model that represents a rate compared to a base
 * 
 * @author tyler
 *
 */
public class RateModel {

	/** The name of the currency */
	private String currency;

	/** The rate of the currency compared to the base */
	private double rate;

	/**
	 * Constuctor
	 * 
	 * @param currency
	 * @param rate
	 */
	public RateModel(String currency, double rate) {
		this.currency = currency;
		this.rate = rate;
	}

	// ACCESSORS
	public String getCurrency() {
		return currency;
	}

	public double getRate() {
		return rate;
	}

	// MUTATORS
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	
}
