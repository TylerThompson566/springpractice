package com.tylert.springpractice.exchangerates.model;

import java.util.List;

/**
 * The model that represents what our service sends back for /rates calls
 * 
 * @author tyler
 *
 */
public class RatesResponseModel {

	/** The base for currency conversion */
	private String base;
	
	/** amount of the base currency */
	private double amount;

	/** The date the request was made on */
	private String date;

	/** The list of rates */
	private List<RateModel> rates;

	/**
	 * constuctor
	 * 
	 * @param base
	 * @param rates
	 * @param date
	 */
	public RatesResponseModel(String base, double amount, String date, List<RateModel> rates) {
		this.base = base;
		this.amount = amount;
		this.date = date;
		this.rates = rates;
	}

	// ACCESSORS
	public String getBase() {
		return base;
	}
	
	public double getAmount() {
		return amount;
	}

	public String getDate() {
		return date;
	}

	public List<RateModel> getRates() {
		return rates;
	}

	// MUTATORS
	public void setBase(String base) {
		this.base = base;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setRates(List<RateModel> rates) {
		this.rates = rates;
	}
	
	

	
}
