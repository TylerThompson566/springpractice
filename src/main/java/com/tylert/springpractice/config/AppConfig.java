package com.tylert.springpractice.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * This class is the configuration for the Rest Template
 * 
 * @author tyler
 *
 */
@Configuration
public class AppConfig {

	/**
	 * Get the bean for the RestTemplate
	 * 
	 * @param builder the Rest Template Builder
	 * @return the RestTemplate
	 */
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
